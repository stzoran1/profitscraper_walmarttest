import sys, getopt
print(sys.version)

from data_feed import CheckoutDataFeed

#For debugging only
import time

from walmart_checkout import WalmartCheckout

def main(argv):
    #Parse arguments from console
    order_id = '123456'
    endpoint = 'http://api.santogigliotti.com/autoOrdering/testwalmart'
    try:
        opts, args = getopt.getopt(argv,"he:o:",["endpoint=","orderid="])
    except getopt.GetoptError:
        print('main.py -e <endpoint> -o <orderid>')
        print('main.py --endpoint=<endpoint> --orderid=<orderid>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('main.py -e <endpoint> -o <orderid>')
            print('main.py --endpoint=<endpoint> --orderid=<orderid>')
            sys.exit()
        elif opt in ("-e", "--endpoint"):
            endpoint = arg
        elif opt in ("-o", "--orderid"):
            order_id = arg
    print 'API endpoint is ', endpoint
    print 'Ebay order ID is ', order_id

    if endpoint == '':
        print("Error: Endpoint must be defined.")
        sys.exit()

    if order_id == '':
        print("Error: Order ID must be defined.")
        sys.exit()

    #Separate class to handle data API
    checkout = CheckoutDataFeed()
    result = checkout.getCheckouts(endpoint, order_id)

    items = result['items']

    #Start Chrome only if there is something to process
    items_count = len(items)

    if items_count == 0:
        print('Error: No items found.')
    else:

        username = result['user']['email']

        walmart = WalmartCheckout(username, order_id, result['address'], result['delete_address'])

        #First login
        #Avoid using cookies, but rather login each time
        password = result['user']['password']
        walmart.login(password)

        #After login check is there old cart items and delete them
        walmart.emptyOldCart()

        #Add each item in cart
        for attribute, item in items.iteritems():
            
            checkoutResult = walmart.itemCheckout(attribute,item['qty'])
            #If any of items is not available from walmart as seller or quantity not available stop the process and exit
            if checkoutResult < 0:
                print("SKU " + attribute + " checkout failed with code: " + str(checkoutResult))
                sys.exit(1)

        #Final checkout
        #If everything is fine until the end cookie will be saved
        total_items = int(result['total_items'])
        total_quantity = int(result['total_quantity'])
        checkoutResult = walmart.cartCheckOut(result['user']['cvv'], result['price'], total_items, total_quantity)
        
        if checkoutResult < 0:
            print("Checkout failed with code: " + str(checkoutResult))
            sys.exit(2)

if __name__ == "__main__":
   main(sys.argv[1:])