import urllib2
import json

#Used to handle data API
class CheckoutDataFeed:

    def getCheckouts(self, url, ebay_order_id):
        url = url + "/" + str(ebay_order_id)
        content = urllib2.urlopen(url).read()
        content = json.loads(content)
        return content