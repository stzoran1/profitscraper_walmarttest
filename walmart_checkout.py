from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
import pickle
import json
import os

#For debugging only
import time

class WalmartCheckout:

    def __init__(self, username, order_id, address, delete_address):

        if delete_address == '1':
            self.delete_address = True
        else:
            self.delete_address = False

        self.username = username
        self.order_id = order_id

        #Parse address
        fullName = address[0]
        fullName = fullName.split(" ")

        self.first_name = fullName[0]
        if (len(fullName)) > 1:
            self.last_name = ' '.join(fullName[1:])
        else:
            self.last_name = fullName[0]
        self.address = address[1]
        self.appartment = address[2]
        self.city = address[3]
        self.state = address[4]
        self.zip = address[5]
        self.phone = address[6]

        #Extract script root path
        script_path = os.path.dirname(os.path.abspath(__file__))

        self.cookies_path = script_path + "/cookies"
        self.screenshots_path = script_path + "/screenshots"

        self.createDirectory(self.cookies_path)
        self.createDirectory(self.screenshots_path)

        #Load env specific settings from file
        try:
            with open('settings.json') as jsonDataFile:
                settings = json.load(jsonDataFile)
        except:
            print("Please create valid settings.json file according to example provided in project root directory.")
            raise Exception

        #Start maximized in order to make screenshot
        options = webdriver.ChromeOptions()
        options.add_argument('--start-maximized')
        self.driver = webdriver.Chrome(settings['chromedriver_path'], options = options)       
    
    def loadSession(self):
        #Navigate to non-existing page in order to load cookies
        #Non-existing page is used to speed up loading as it usually doesn't have content
        self.driver.get("https://www.walmart.com/product/loadcookies/sellers")
        
        #Load session cookies if exist
        if self.loadCookies(self.username) == True:
            #Check is profile page loaded to confirm session is granted with loaded cookies
            try:
                self.driver.get("https://www.walmart.com/account/login?ref=domain")
                waitToLoad = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.LINK_TEXT, "Change your password")))
                return True
            except:
                return False
        else:
            return False

    #Login handler
    def login(self, password):
        self.password = password
        self.driver.get("https://www.walmart.com/account/login?ref=domain")
        txtUserName = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "email")))
        txtPassword = self.driver.find_element_by_id("password")

        txtUserName.send_keys(self.username)
        txtPassword.send_keys(password)

        btnSignIn = self.driver.find_element_by_css_selector("[data-automation-id='signin-submit-btn']")
        btnSignIn.click()
        #Prevent next operation to happen before signin process is properly done.
        waitToLoad = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.LINK_TEXT, "Change your password")))
        #Save cookies after session is granted
        self.saveCookies(self.username)

    # Single item chekout
    #  0 - OK
    # -1 - Seller not found
    # -2 - Quantity not available
    def itemCheckout(self, sku, qty):
        qty = str(qty)
        url = "https://www.walmart.com/product/" + sku + "/sellers"
        
        self.driver.get(url)
        
        print("SKU:" + sku)
        print("URL:" + url)

        divSellers = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CLASS_NAME, "MarketplaceSellers")))

        divSellersCards = divSellers.find_elements(By.CSS_SELECTOR,"[data-tl-id^='ProductSellerCard-']")

        if len(divSellersCards)>0:
            for divSellerCard in divSellersCards:
                aSellerName = divSellerCard.find_element_by_css_selector("[data-tl-id='ProductSellerCardSellerInfo-SellerName']")
                
                if aSellerName.text == "Walmart":
                    ddQty = Select(divSellerCard.find_element_by_css_selector("[data-automation-id='field']"))
                    try:
                        ddQty.select_by_visible_text(qty)
                    except:
                        #Make screenshot
                        self.saveScreenShot(self.order_id)
                        self.savePageHtml(self.order_id)
                        return -2

                    btnAddToCart = divSellerCard.find_element_by_css_selector("[data-automation-id='button']")
                    btnAddToCart.click()

                    return 0

            #Make screenshot
            self.saveScreenShot(self.order_id)
            self.savePageHtml(self.order_id)
            return -1
        else:
            #Make screenshot
            self.saveScreenShot(self.order_id)
            self.savePageHtml(self.order_id)
            #Error code - Seller not found
            return -1
    
    # Shopping cart checkout
    #  0 - OK
    # -1 - maxGrandTotal Exceeded
    def cartCheckOut(self, cvv, maxGrandTotal, totalItems, totalQuantity):
        maxGrandTotal = float(maxGrandTotal)

        self.driver.get("https://www.walmart.com/cart")
        #Chrome cashes some content so refresh is needed to get fresh data
        self.driver.refresh()
        btnCheckOutWait = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[class='hide-content-max-m']")))
        
        #Double check total quantity and items count
        divCartItems = WebDriverWait(self.driver, 30).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "[data-automation-id^='cart-item-row-']")))
        cartItemsCount = len(divCartItems)

        if (cartItemsCount != totalItems):
            print("ERROR:ITEM_COUNT_NOT_MATCHING.")
            #Make screenshot
            self.saveScreenShot(self.order_id)
            self.savePageHtml(self.order_id)
            return -2

        cartQty = 0
        if cartItemsCount > 0:
            for divCartItem in divCartItems:
                cmbQty = Select(divCartItem.find_element_by_css_selector("[data-automation-id='cart-item-qty-chooser']"))
                itemQty = int(cmbQty.first_selected_option.text)
                cartQty += itemQty
        
        if (cartQty != totalQuantity):
            print("ERROR:QUANTITY_NOT_MATCHING.")
            #Make screenshot
            self.saveScreenShot(self.order_id)
            self.savePageHtml(self.order_id)
            return -3
        #***********************************************
        
        btnCheckOut = btnCheckOutWait.find_element(By.CSS_SELECTOR, "[data-automation-id='cart-pos-proceed-to-checkout']")
        btnCheckOut.click()

        #Continue to addresses
        btnContinue = WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-automation-id='fulfillment-continue']")))
        #spinner spinner-large spinner-colorize spinner-fixed
        #Wait for spinner to disspear
        WebDriverWait(self.driver, 30).until(EC.invisibility_of_element((By.CSS_SELECTOR, "[class='spinner spinner-large spinner-colorize spinner-fixed']")))
        btnContinue.click()

        #Add new address
        btnAddNewAddress = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[data-automation-id='new-address-tile-add-new-address']")))
        btnAddNewAddress.click()

        txtFirstName = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-firstName']")
        txtFirstName.send_keys(self.first_name)

        txtLastName = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-lastName']")
        txtLastName.send_keys(self.last_name)

        txtStreetAddressName = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-addressLineOne']")
        txtStreetAddressName.send_keys(self.address)

        txtAptSuiteEtc = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-addressLineTwo']")
        txtAptSuiteEtc.send_keys(self.appartment)

        txtShippingPhone = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-shippingPhone']")
        txtShippingPhone.send_keys(self.phone)

        txtCity = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-city']")
        txtCity.send_keys(self.city)

        txtState = Select(self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-state']"))
        txtState.select_by_value(self.state)

        txtPostalCode = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-postalCode']")
        txtPostalCode.clear()
        txtPostalCode.send_keys(self.zip)

        btnSaveAddress = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-submit']")
        btnSaveAddress.click()

        #Wait to check if warning message is diplayed
        #Accept address on validation error
        try:
            spanWarning = WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "[data-automation-id='address-form-alert']")))
            btnSaveAddress = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='address-form-submit']")
            btnSaveAddress.click()
        except:
            pass

        #Continue to payment
        btnContinue = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[data-automation-id='address-book-action-buttons-on-continue']")))
        #Wait for spinner to disspear
        WebDriverWait(self.driver, 30).until(EC.invisibility_of_element((By.CSS_SELECTOR, "[class='spinner spinner-large spinner-colorize spinner-fixed']")))
        btnContinue.click()

        #Enter CVV
        txtCVV = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.NAME, "cvv")))
        txtCVV.send_keys(cvv)

        btnReviewYourOrder = self.driver.find_element_by_css_selector("[data-automation-id='submit-payment-cc']")
        btnReviewYourOrder.click()

        #Check total
        txtGrandTotal = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[data-automation-id='pos-grand-total-amount']")))

        strGrandTotal = txtGrandTotal.text
        
        print("COST:" + strGrandTotal)

        #Remove dollar sign and comma
        strGrandTotal = strGrandTotal[1:]
        strGrandTotal = strGrandTotal.replace(",","")

        grandTotal = float(strGrandTotal)

        #Check totals
        if grandTotal <= maxGrandTotal:
            btnPlaceOrderWait = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[data-automation-id='summary-place-holder']")))
            btnPlaceOrder = self.driver.find_element(By.CSS_SELECTOR, "[data-automation-id='summary-place-holder']")
            btnPlaceOrder.click()
            #After this click we assume checkout is done as payment can't be authorized with dummy CVV

            #Save cookies
            self.saveCookies(self.username)
            #Make screenshot
            self.saveScreenShot(self.order_id)
            self.savePageHtml(self.order_id)
            print("ORDERID:UNKNOWN")

            #Delete address
            if self.delete_address == True:
                self.deleteAddresses()
                
            print("COMPLETE")
            return 0
        else:
            #Make screenshot
            self.saveScreenShot(self.order_id)
            self.savePageHtml(self.order_id)
            print("ERROR:TOTAL_PRICE_EXCEEDED")
            return -1

    #Save cookies to defined file for each user
    def saveCookies(self, file_name):
        if file_name == '':
            file_name = time.strftime("%Y%m%d-%H%M%S")
        file_path = self.cookies_path + "/" + file_name
        pickle.dump(self.driver.get_cookies(), open(file_path + ".pkl","wb"))

    #Load cookies
    def loadCookies(self, file_name):
        try:
            file_path = self.cookies_path + "/" + file_name
            cookies = pickle.load(open(file_path + ".pkl", "rb"))
            for cookie in cookies:
                self.driver.add_cookie(cookie)
            return True
        except:
            return False
       
    #Check old cart and empty if any item left from previous sessions    
    def emptyOldCart(self):
        self.driver.get("https://www.walmart.com/cart")

        bubbleCart = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[data-tl-id='header-GlobalHeaderBubblesNav-Cart-BubbleLink']")))
        #Try to detect number of items in cart
        oldCartItems = 0
        try:
            bubbleCartCount = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[class='HeaderCartCount font-bold']")))
            oldCartItems = int(bubbleCartCount.text)
        except:
            pass
        
        if oldCartItems > 0:
            divCartItems = self.driver.find_elements(By.CSS_SELECTOR,"[data-automation-id^='cart-item-row-']")

            if len(divCartItems)>0:
                for divCartItem in divCartItems:
                    btnRemove = divCartItem.find_element_by_css_selector("[data-automation-id='cart-item-remove']")
                    #Wait for spinner to disspear
                    WebDriverWait(self.driver, 30).until(EC.invisibility_of_element((By.CSS_SELECTOR, "[class='Cart-SpinnerOverlay Cart-RootContentSpinner fixed']")))
       
                    btnRemove.click()

    #Save PNG screenshot of page in /screenshots folder
    def saveScreenShot(self, file_name):
        if file_name == '':
            file_name = time.strftime("%Y%m%d-%H%M%S")
        file_path = self.screenshots_path + "/" + file_name + ".png"
        self.driver.get_screenshot_as_file(file_path)

    #Save HTML of entire page in /screenshots folder
    def savePageHtml(self, file_name):
        if file_name == '':
            file_name = time.strftime("%Y%m%d-%H%M%S")
        file_path = self.screenshots_path + "/" + file_name + ".html"
        html = self.driver.page_source.encode('utf-8')

        f = open(file_path, 'w')
        f.write(html)

    #Create directory if doesn't exist
    def createDirectory(self, directory_path):
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

    #Delete all address from account for given address line
    def deleteAddresses(self):
        self.driver.get("https://www.walmart.com/account/shippingaddresses")
        #Wait page to load
        divsAddr = WebDriverWait(self.driver, 30).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "flex-wrap")))
        self.delAddr()
    
    def delAddr(self):
        divsAddr = self.driver.find_elements(By.CLASS_NAME, "flex-wrap")
        if len(divsAddr) > 0:
            for divAddr in divsAddr:
                try:
                    details = divAddr.find_element(By.CLASS_NAME, "address-book-details").get_attribute("innerHTML")
                    address = self.address.strip()
                    if address in details:
                        btnDelete = divAddr.find_element(By.CSS_SELECTOR, "[data-automation-id^='delete-address-']")
                        btnDelete.click()
                        btnConfirm = WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[data-automation-id^='confirm-delete-']")))
                        btnConfirm.click()
                        print("ADDRESS_DELETED:" + address)
                        #This pause prevents additional login to pop up
                        time.sleep(1)
                        self.delAddr()
                except:
                    pass
        

    def __del__(self):
        self.driver.quit()

    


